/**********************************************************************
 * Split Ring Host Side Communcation                                   *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: November 11, 2017                                          *
 * Modified: November 11, 2017                                         *
 ***********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <inttypes.h>
#include "generalFunctions.h"
#include "printingFunctions.h"
#include "Nahanni.h"
#include "ringBuffer.h"

extern bool run;

int main() {
	run = true;

	//init nahanni
	Nahanni *NN = newNahanni("/dev/shm/orange", 256, 1, HOST);

	//init ring
	ringBuff *Input = newBuffer(2048, &NN->Memory, HOST, true);
	ringBuff *Output = newBuffer(2048, &NN->Memory, HOST, true);
	uint64_t x = 0;
	while (run) {
		setString(Output, "AbCdEfGhIjKlMnOpQrStUvWxYz", 26, HOST); 
		printf("%"PRId64"\n", get64(Input, HOST));
		x++;
	}

	freeNahanni(NN);
}
