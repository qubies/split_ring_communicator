/**********************************************************************
 * Split Ring Guest Side Increment Test                                *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: November 23, 2017                                          *
 * Modified: November 23, 2017                                         *
 ***********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <inttypes.h>
#include <string.h>
#include "generalFunctions.h"
#include "printingFunctions.h"
#include "Nahanni.h"
#include "ringBuffer.h"
#include "hiredis/hiredis.h"      
#include "redisConnect.h"

#define NUM_LOOPS 1000000

extern bool run;

int main() {
	run = true;
	
	//init nahanni
	Nahanni *NN = newNahanni("/dev/uio0", 256, 2, GUEST);

	//init ring
	ringBuff *Input = newBuffer(2048, &NN->Memory, GUEST, false);
	ringBuff *Output = newBuffer(2048, &NN->Memory, GUEST, false);

	///NOTE THAT THERE IS NO CONNECTION TO REDIS HERE
	int x = 0;
	int32_t resp = 0;
	while (run && x < NUM_LOOPS) {
		//we ask for an incremnt
		setCommand(Input, FETCH);

		//we set the key first 
		setString(Input, "Key1", 5, GUEST);
		
		//we watch for our response
		resp = getResponse(Output);

		if (resp == FETCH) {
			char * newval = getString(Output, GUEST);
			debugPrint("Value: %s\n", newval);
			free(newval);
		} else {
			// an error occurred....
			debugPrint("Response: %d\n", resp);
		}
		x++;
	}
	freeNahanni(NN);
	
}

