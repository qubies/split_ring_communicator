/**********************************************************************
 * Split Ring Printing Functions                                       *
 * A collection of helpful information output utilities                *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: November 11, 2017                                          *
 * Modified: November 11, 2017                                         *
 ***********************************************************************/

#include <stdbool.h>

extern bool run;

// the colors accepted by colorprint
enum COLORS {
	COLOR_NONE = 0,
	BLACK_FG = 30,
	RED_FG = 31,
	GREEN_FG = 32,
	YELLOW_FG = 33,
	BLUE_FG = 34,
	MAGENTA_FG = 35,
	CYAN_FG = 36,
	WHITE_FG = 37,
	BLACK_BG = 40,
	RED_BG = 41,
	GREEN_BG = 42,
	YELLOW_BG = 43,
	BLUE_BG = 44,
	MAGENTA_BG = 45,
	CYAN_BG = 46,
	WHITE_BG = 47
};

// like errPrint, debug print prints debug information when DEBUG is defined.
void debugPrint(const char *str, ...);

// variadic wrapper for fprintf(stderr,....) anything you can pass to a printf, this functino is the same but for
// stderr) ONLY PRINTS WHEN ERRPRINT is defined.
void errPrint(const char *str, ...);

// colorPrint prints out to terminal using ANSI color codes
// COLOR codes are defined in the enum above, use _BG for bg and _FG for fg
void colorPrint(int fg, int bg, char *str, ...);

//printSeparator prints a split for output separation using char, num times
void printSeparator(char sep, int num);

//prints the chars in a column of padding wide, with char as separator in between columns
//the variadic requires the same number of arguments as the number of columns
void columnPrint(int numCols, char sep, bool withSep, int padding, ...);
