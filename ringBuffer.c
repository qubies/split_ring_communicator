/**********************************************************************
 * Split Ring Buffer                                                   *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: November 11, 2017                                          *
 * Modified: November 11, 2017                                         *
 ***********************************************************************/

#include "generalFunctions.h"
#include "printingFunctions.h"
#include "ringBuffer.h"
#include <stdint.h>
#include <inttypes.h>
#include "Nahanni.h"
#include <stdlib.h>
#include <stdbool.h>
extern bool run;

static bool isPowerOfTwo(uint64_t val) {
	return (val != 0) && (val & (val -1)) == 0;
}
ringBuff *newBuffer(uint64_t capacity, void **memory, int guestOrHost, bool initialize) {	
	if (!isPowerOfTwo(capacity)) {
		errPrint("Capacity must be a power of 2\n");
		exit(EXIT_FAILURE);
	}
	//cast the buffer struct
	ringBuff *R = ((ringBuff *) *memory);

	//init
	*memory =  R + 1;
	if (initialize) {
		R->head = 0;
		R->tail = 0;
		R->size = capacity;
		R->command = 0;
		R->response = 0;
	}
	
	if (guestOrHost == HOST) {
		R->contents = (uint8_t *)*memory;
		*memory = R->contents + capacity;
	}

	if (guestOrHost == GUEST) {
		R->guestContents = (uint8_t *)*memory;
		*memory = R->guestContents + capacity;
	}

	debugPrint("Ring Buffer Created\n");
	debugPrint("Size: %"PRId64"\n", R->size);
	debugPrint("R: %p\n", R);
	debugPrint("Contents: %p\n", R->contents);
	debugPrint("Guest Contents: %p\n", R->guestContents);
	return R;
}

//masking idea taken from https://www.snellman.net/blog/archive/2016-12-13-ring-buffers/
static uint64_t index(ringBuff *R, uint64_t val) {
	return val & (R->size -1);
}
static bool isEmpty(ringBuff *R) {
	return R->head == R->tail;
}

static uint64_t currentSize(ringBuff *R) {
	return R->head - R->tail;
}
static bool isFull(ringBuff *R) {
	return currentSize(R) == R->size;
}


void add(ringBuff *R, uint8_t data, int guestOrHost) {
	while (isFull(R)) {
		//spin
	}
	
	if (guestOrHost == HOST) {
		R->contents[index(R, R->head)] = data;
	} else {
		R->guestContents[index(R, R->head)] = data;
	}
	R->head ++;
}

uint8_t consume(ringBuff *R, int guestOrHost) {
	while (isEmpty(R)) {
		//we cant consume so spin.
	}
	if (guestOrHost == HOST) {
		return R->contents[index(R, R->tail++)];
	}

	return R->guestContents[index(R, R->tail++)];

}

uint64_t get64(ringBuff *R, int guestOrHost) {
	//make some space on the heap
	uint8_t buf[8];
	//get the ints
	for (int x = 0; x < 8; x++) {
		buf[x] = consume(R, guestOrHost);
	}
	uint64_t *newInt = (uint64_t*)buf;
	return *newInt;
}

void set64(ringBuff *R, uint64_t val, int guestOrHost) {
	//get the ints
	uint8_t *vals = (uint8_t *)&val;
	for (int x = 0; x < 8; x++) {
		add(R, vals[x], guestOrHost);
	}
}

char * getString(ringBuff *R, int guestOrHost) {
	uint64_t len = get64(R, guestOrHost);
	char * retVal = allocationCheck(malloc((int)len));
	for (uint64_t x = 0; x < len; x++) {
		retVal[x] = consume(R, guestOrHost);
	}
	return retVal;
}

void setString(ringBuff *R, char * str, uint64_t len, int guestOrHost) {
	/*if (len > R->size-8) { //-8 because the len is a 64bit uint
		//this is likely unnecessary, as long as the component read comes along...
		errPrint("String too large to be placed in array.");
		return;
	}*/
	//set the length first:
	set64(R, len, guestOrHost);

	//now set the string  bytes
	uint8_t *vals = (uint8_t *)str;
	for (uint64_t x = 0; x < len; x++) {
		add(R, vals[x], guestOrHost);
	}
}

static void setCom(int32_t * address, int32_t val) {
	while (*address != 0) {
		//spin
	}
	*address = val;
}

static int32_t getCom(int32_t * address) {
	while (*address == 0) {
		//spin....
	}
	int32_t temp = *address;
	*address = 0;
	return temp;
}
void setCommand(ringBuff *R, int32_t command) {
	setCom(&R->command, command);
}

void setResponse(ringBuff *R, int32_t response) {
	setCom(&R->response, response);
}

int32_t getResponse(ringBuff *R) {
	return getCom(&R->response);
}

int32_t getCommand(ringBuff *R) {
	return getCom(&R->command);
}
