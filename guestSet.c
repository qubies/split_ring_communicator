/**********************************************************************
 * Split Ring Guest Side Increment Test                                *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: November 23, 2017                                          *
 * Modified: December 5, 2017                                         *
 ***********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <inttypes.h>
#include <string.h>
#include <pthread.h>

#include "generalFunctions.h"
#include "printingFunctions.h"
#include "Nahanni.h"
#include "ringBuffer.h"
#include "hiredis/hiredis.h"      
#include "redisConnect.h"

#define NUM_LOOPS 1000000

extern bool run;

//thread struct
typedef struct {
	ringBuff * buff;
	ringBuff * reg;
}Buff;

//threaded function, sets the values in the outbound (guest->host) buffer
void *setVals (void *buffers) {
	int x = 0;
	Buff * myBuff = (Buff *) buffers;
	while (run && x < NUM_LOOPS) {
		set64(myBuff->reg, SET, GUEST);
		setString(myBuff->buff, "Key1", 5, GUEST);

		char val[10];
		snprintf(val,10, "%d", x);
		int len = strlen(val);
		setString(myBuff->buff, val, len, GUEST);
		x++;
	}
	printf("DONE\n");
	return NULL;
}

int main() {
	run = true;
	//init nahanni
	Nahanni *NN = newNahanni("/dev/uio0", 256, 2, GUEST);

	//init ring
	ringBuff *Input = newBuffer(2048, &NN->Memory, GUEST, false);
	ringBuff *Output = newBuffer(2048, &NN->Memory, GUEST, false);

	ringBuff *command = newBuffer(32, &NN->Memory, GUEST, false);
	ringBuff *response = newBuffer(32, &NN->Memory, GUEST, false);
	///NOTE THAT THERE IS NO CONNECTION TO REDIS HERE
	int x = 0;
	uint64_t resp = 0;
	pthread_t setter;
	
	Buff * rb  = malloc(sizeof(Buff));
	rb->buff = Input;
	rb->reg = command;

	if (pthread_create(&setter, NULL, setVals, rb)) {
		printf("Error Creating Thread\n");
	}

	while (run && x < NUM_LOOPS) {

		resp = get64(response, GUEST);

		if (resp == SET) {
			//we have success... 
			char * newval = getString(Output, GUEST);
			free(newval);
		} else {
			// an error occurred....
			debugPrint("Response: %d\n", resp);
		}
		x++;
	}
	printf("Done\n");
	pthread_join(setter, NULL);
	free(rb);
	freeNahanni(NN);
}

