/**********************************************************************
 * Split Ring Host Side Increment Test                                 *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: November 23, 2017                                          *
 * Modified: November 23, 2017                                         *
 ***********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <inttypes.h>
#include <hiredis/hiredis.h>
#include <string.h>
#include "generalFunctions.h"
#include "printingFunctions.h"
#include "Nahanni.h"
#include "ringBuffer.h"
#include "redisConnect.h"

extern bool run;


int main() {
	run = true;

	//init nahanni
	Nahanni *NN = newNahanni("/dev/shm/orange", 256, 1, HOST);

	//init ring
	ringBuff *Input = newBuffer(2048, &NN->Memory, HOST, true);
	ringBuff *Output = newBuffer(2048, &NN->Memory, HOST, true);
	
	//connect to redis
	redisContext *c =redisNewConnect("127.0.0.1", 6379);
	while (run) {
		//get the command
		uint32_t action = getCommand(Input);
		switch (action) {
			case INCR: ;
				//we need to know which variable
				uint64_t incrementResponse = 0;
				char * var = getString(Input, HOST);
				//we try to incrememnt it in redis
				incrementResponse = increment(c, var);	
				free(var);
				if (incrementResponse > 0) {
					//we set our response to correct
					setResponse(Output, INCR);
					//we place the response in the output buffer
					set64(Output, incrementResponse, HOST);
						
				} else {
					//there was an error, we set the error
					setResponse(Output, GEN_ERR);
					//we place nothing in the output buffer.
					errPrint("Increment returned negative or zero\n");
				}
				break;
			case SET: ;
				  char * key = getString(Input, HOST);
				  char * val = getString(Input, HOST);
				  char *response = setKey(c, key, val);
				  free(key);
				  free(val);
				  if (incrementResponse > 0) {
					  setResponse(Output, SET);
					  setString(Output, response, strlen(response));
				  } else {
					  setResponse(Output, GEN_ERR);
					  errPrint("Set returned an error\n");
				  }
		}
	}
	redisFree(c);
	freeNahanni(NN);
}
