#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <inttypes.h>
#include "generalFunctions.h"
#include "printingFunctions.h"
#include "Nahanni.h"
#include "ringBuffer.h"

extern bool run;

int main() {
	run = true;
	
	//init nahanni
	Nahanni *NN = newNahanni("/dev/uio0", 256, 2, GUEST);

	//init ring
	ringBuff *Input = newBuffer(2048, &NN->Memory, GUEST, false);
	ringBuff *Output = newBuffer(2048, &NN->Memory, GUEST, false);
	
	uint64_t x = 0;

	while (run) {

		char *tmp = getString(Output, GUEST);
		printf("%s\n", tmp);
		free(tmp);
		set64(Input, x, GUEST);
		x++;
	}
	freeNahanni(NN);
	
}
