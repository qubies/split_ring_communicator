/**********************************************************************
 * Split Ring General Functions                                        *
 * General functions is a list of useful helper functions              *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: September 19, 2017                                         *
 * Modified: November 11, 2017                                         *
 ***********************************************************************/
#include <time.h>
// initializes signal handling
void createHandler(void);

//reset string frees a heap string, then sets it to NULL for safety
void resetString(char *str);

// wrapper function that checks for allocation failures on malloc/calloc
// allocation check terminates on allocation failure with EXIT_FAILURE
void *allocationCheck(void *val);

// checks for NULL and frees used heap allocated strings
void strFree(char *str);

//nanoSleep allows fine grained control of the sleep function
void nanoSleep(long nanoseconds); 
