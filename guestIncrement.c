/**********************************************************************
 * Split Ring Guest Side Increment Test                                *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: November 14, 2017                                          *
 * Modified: December 5, 2017                                         *
 ***********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <inttypes.h>
#include "generalFunctions.h"
#include "printingFunctions.h"
#include "Nahanni.h"
#include "ringBuffer.h"
#include "hiredis/hiredis.h"      
#include "redisConnect.h"

#define NUM_LOOPS 1000000

extern bool run;

int main() {
	run = true;
	
	//init nahanni
	Nahanni *NN = newNahanni("/dev/uio0", 256, 2, GUEST);

	//init ring
	ringBuff *Input = newBuffer(2048, &NN->Memory, GUEST, false);
	ringBuff *Output = newBuffer(2048, &NN->Memory, GUEST, false);

	//init buffered registers
	ringBuff *command = newBuffer(32, &NN->Memory, GUEST, false);
	ringBuff *response = newBuffer(32, &NN->Memory, GUEST, false);

	///NOTE THAT THERE IS NO CONNECTION TO REDIS HERE
	
	int x = 0;
	uint64_t resp = 0;
	uint64_t cVal = 0;

	while (run && x < NUM_LOOPS) {

		//we ask for an incremnt
		set64(command, INCR, GUEST);
		//we set the value in the output buffer:
		setString(Input, "z", 2, GUEST);
		//we watch for our response
		resp = get64(response, GUEST);

		if (resp == INCR) {
			//we have success... read the increment and print it
			cVal = get64(Output, GUEST);
		} else {
			// an error occurred....
			debugPrint("Response: %d\n", resp);
		}
		x++;
	}

	debugPrint("Final Value: %"PRId64"\n", cVal);
	freeNahanni(NN);
}

