#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Nahanni.h"
#include "ringBuffer.h"
#include "printingFunctions.h"
#include <hiredis/hiredis.h>
#include "redisConnect.h"
#define INCR 1
#define SET 2
#define FETCH 3
#define GEN_ERR -1
extern bool run;

int main(int argc, char **argv) {
	redisContext *c;
	redisReply *reply;
	const char *hostname = (argc > 1) ? argv[1] : "127.0.0.1";
	int port = (argc > 2) ? atoi(argv[2]) : 6379;

	struct timeval timeout = { 1, 500000 }; // 1.5 seconds
	c = redisConnectWithTimeout(hostname, port, timeout);
	if (c == NULL || c->err) {
		if (c) {
			printf("Connection error: %s\n", c->errstr);
			redisFree(c);
		} else {
			printf("Connection error: can't allocate redis context\n");
		}
		exit(1);
	}

	for (int x = 0; x <1000000; x ++) {
		char val[10];
		snprintf(val,10, "%d", x);
		reply = redisCommand(c, "SET %s %s","TestKey", val);
		freeReplyObject(reply);
	}

	/* Disconnects and frees the context */
	redisFree(c);

	return 0;
}
