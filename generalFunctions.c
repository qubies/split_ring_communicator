/**********************************************************************
 * Split Ring General Functions                                        *
 * General functions is a list of useful helper functions              *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: September 19, 2017                                         *
 * Modified: November 11, 2017                                         *
 ***********************************************************************/


#include <string.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <stdio.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>

#include "printingFunctions.h"


bool run = true;

static void sigHandler(int signal) {
	if (signal == SIGINT) {
		// set run to false on signal interrupt (ctrl + c)
		run = false;
	}
}

void createHandler(void) {
	struct sigaction doAction;

	// Initialize handler
	memset(&doAction, 0, sizeof(struct sigaction));
	sigemptyset(&doAction.sa_mask);

	// assign the function to the sigaction struct
	doAction.sa_handler = sigHandler;

	// no flags needed
	doAction.sa_flags = 0;

	// error check on signal handler assignments
	if (sigaction(SIGINT, &doAction, 0) == -1) {
		perror("SIGINT Error");
	}

	if (sigaction(SIGCHLD, &doAction, 0) == -1) {
		perror("SIGCHLD Error");
	}
}


void resetString(char *str) {
	free(str);
	str = NULL;
}

void *allocationCheck(void *val) {
	if (val == NULL) {
		errPrint("A memory allocation (IE malloc, calloc...) Failed. EXITING.\n");
		exit(EXIT_FAILURE);
	}
	return val;
}

void strFree(char *SP) {
	if (SP != NULL) {
		free(SP);
	}
}

void nanoSleep(long nanoSeconds) {
	nanosleep((const struct timespec[]) {
		{
			0, nanoSeconds
		}
	}, NULL);
}
