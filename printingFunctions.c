/**********************************************************************
 * Split Ring Printing Functions                                       *
 * A collection of helpful information output utilities                *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: November 11, 2017                                          *
 * Modified: November 11, 2017                                         *
 ***********************************************************************/

#include <stdarg.h>
#include <stdio.h>
#include <stdbool.h>

#define DEBUG
#define ERRPRINT

void debugPrint(const char *str, ...) {
#ifdef DEBUG
	va_list args;

	va_start(args, str);
	vfprintf(stderr, str, args);
	va_end(args);
#endif
}

void errPrint(const char *str, ...) {
#ifdef ERRPRINT
	va_list args;

	va_start(args, str);
	vfprintf(stderr, str, args);
	va_end(args);
#endif
}

void colorPrint(int fg, int bg, char *str, ...) {
	va_list args;
	va_list argsCPY;

	va_start(args, str);
	va_start(argsCPY, str);

	int len = vsnprintf(NULL, 0, str, args) + 1;

	char theString[len];

	vsprintf(theString, str, argsCPY);

	if (bg != 0) {
		printf("\033[%d;%d;1m%s\033[0m", fg, bg, theString);
	} else {
		printf("\033[%d;1m%s\033[0m", fg, theString);

	}
}

void printSeparator(char sep, int num) {
	for (int x = 0; x < num; x++) {
		printf("%c", sep);
	}
	printf("\n");
}

void columnPrint(int numCols, char sep, bool withSep, int padding, ...) {
	va_list args;
	va_start(args, padding);
	if (withSep) {
		printSeparator('-', padding * numCols + numCols * 2 + 1);
	}
	for (int x = 0; x < numCols; x++) {
		printf("%c %-*s", sep, padding, va_arg(args, char *));
	}
	printf("%c\n", sep);
	if (withSep) {
		printSeparator('-', padding * numCols + numCols * 2  + 1);
	}

}

