/**********************************************************************
 * Split Ring Buffer                                                   *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: November 11, 2017                                          *
 * Modified: November 11, 2017                                         *
 ***********************************************************************/

#include <stdint.h>
#include <stdbool.h>

typedef struct {
	uint64_t head;
	uint64_t tail;
	uint64_t size;
	uint8_t *contents;
	uint8_t *guestContents;
	int32_t command; //the command issued
	int32_t response; // the response feedback
} ringBuff;

//creates a new ringbuffer, and moves the memory pointer along to the end of it
ringBuff * newBuffer(uint64_t capacity, void ** memory, int guestOrHost, bool intialize);

// add a single int into the buffer. 
// This call blocks if the buffer is full.
void add(ringBuff *R, uint8_t data, int guestOrHost);

//consumes a single int from the buffer.
uint8_t consume(ringBuff *R, int guestOrHost);

//consumes the ring as a 64bit uint, and returns the value
uint64_t get64(ringBuff *R, int guestOrHost);

void set64(ringBuff *R, uint64_t val, int guestOrHost); 

void setString(ringBuff *R, char * str, uint64_t len, int guestOrHost);

char * getString(ringBuff *R, int guestOrHost);

int32_t getCommand(ringBuff *R);

int32_t getResponse(ringBuff *R);

void setCommand(ringBuff *R, int32_t command);

void setResponse(ringBuff *R, int32_t response);



