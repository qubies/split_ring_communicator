#ifndef NAHANNI
#define NAHANNI

#define HOST 1
#define GUEST 2

typedef struct {
	char *filePath;
	int fd;
	int size;
	int hostGuest;
	int ID;
	void *Memory;

} Nahanni;


//create a new Nahanni Accessor
Nahanni *newNahanni(char *filePath, int length, int id, int hostGuest);

//auto free the nahanni structure
void freeNahanni(Nahanni *NN);

#endif
