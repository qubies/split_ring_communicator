/**********************************************************************
 *
 * Split Ring Host Side Increment Test                                 *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: November 23, 2017                                          *
 * Modified: December 5, 2017                                         *
 ***********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <inttypes.h>
#include <hiredis/hiredis.h>
#include <string.h>
#include "generalFunctions.h"
#include "printingFunctions.h"
#include "Nahanni.h"
#include "ringBuffer.h"
#include "redisConnect.h"

extern bool run;


int main() {
	run = true;

	//init nahanni
	Nahanni *NN = newNahanni("/dev/shm/orange", 256, 1, HOST);

	//init communication rings
	ringBuff *Input = newBuffer(2048, &NN->Memory, HOST, true);
	ringBuff *Output = newBuffer(2048, &NN->Memory, HOST, true);
	
	//init buffered registers
	ringBuff *command = newBuffer(32, &NN->Memory, HOST, true);
	ringBuff *response = newBuffer(32, &NN->Memory, HOST, true);

	//connect to redis
	redisContext *c = redisNewConnect("127.0.0.1", 6379);
	while (run) {
		//get the command
		uint64_t action = get64(command, HOST);

		switch (action) {
		case INCR:
			;
			//we need to know which variable
			uint64_t incrementResponse = 0;
			char *var = getString(Input, HOST);
			//we try to incrememnt it in redis
			incrementResponse = increment(c, var);
			free(var);
			if (incrementResponse > 0) {
				//we set our response to correct
				//we place the response in the output buffer
				set64(response, INCR, HOST);
				set64(Output, incrementResponse, HOST);

			} else {
				//there was an error, we set the error
				set64(response, GEN_ERR, HOST);
				//we place nothing in the output buffer.
				errPrint("Increment returned negative or zero\n");
			}
			break;

		case SET:
			;
			//collect the values
			char *key = getString(Input, HOST);
			char *val = getString(Input, HOST);

			//call redis
			char *resp = setKey(c, key, val);

			//set the response
			set64(response, SET, HOST);
			setString(Output, resp, strlen(resp), HOST);

			//cleanup
			free(key);
			free(val);
			free(resp);
			break;

		case FETCH:
			;
			//collect values
			char *fetchKey = getString(Input, HOST);
			char *fetchResponse = fetch(c, fetchKey);

			//check for error and cleanup
			free(fetchKey);
			if (fetchResponse  == NULL) {
				setResponse(Output, GEN_ERR);
				free(fetchResponse);
				continue;
			}
			setString(Output, fetchResponse, strlen(fetchResponse), HOST);
			free(fetchResponse);
			break;
		}
	}
	redisFree(c);
	freeNahanni(NN);
}
