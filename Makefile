CC=gcc
CFLAGS = -std=c99 -Wall -pedantic -O3 -D_POSIX_C_SOURCE=200809L -D_BSD_SOURCE $(pkg-config --cflags --libs hiredis)
LDFLAGS =  
#LDLIBS = $(pkg-config --cflags --libs hiredis)
LDLIBS = -lhiredis -lpthread
.PHONY = all clean

all: host guest hostRedis guestIncrement example guestSet setTest guestFetch hostGuest
	
example: example.c

setTest: setTest.c

host: host.c Nahanni.o generalFunctions.o printingFunctions.o ringBuffer.o

guest: guest.c Nahanni.o generalFunctions.o printingFunctions.o ringBuffer.o

hostRedis: hostRedis.c Nahanni.o generalFunctions.o printingFunctions.o ringBuffer.o redisConnect.o

hostGuest: hostGuest.c Nahanni.o generalFunctions.o printingFunctions.o ringBuffer.o redisConnect.o

guestIncrement: guestIncrement.c Nahanni.o generalFunctions.o printingFunctions.o ringBuffer.o redisConnect.o

guestSet: guestSet.c Nahanni.o generalFunctions.o printingFunctions.o ringBuffer.o redisConnect.o

guestFetch: guestFetch.c Nahanni.o generalFunctions.o printingFunctions.o ringBuffer.o redisConnect.o

ringBuffer.o: ringBuffer.c ringBuffer.h generalFunctions.h printingFunctions.h

redisConnect.o: redisConnect.c redisConnect.h

generalFunctions.o: generalFunctions.c generalFunctions.h printingFunctions.h

printingFunctions.o: printingFunctions.c printingFunctions.h

Nahanni.o: Nahanni.c Nahanni.h

clean:
	$(RM) host guest generalFunctions.o Nahanni.o printingFunctions.o ringBuffer.o example hostGuest guestIncrement hostRedis guestSet guestFetch
