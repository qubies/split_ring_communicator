## Split Ring Communicator

#### Split Ring Communicator is a mechanism that uses a lockless ring buffer, through inter-virtual machine shared memory (IVSHMEM) to communicate RPC(Remote Procedure) calls from the guest to the host. 

The Split ring communicator is able to speed up communication that would normally occurr over the network by passing information throguh a shared memory buffer.
This is because a call to the network will incur what is called a VM Exit, a trap to the host os which is necessary for outside communication to take place. When these calls are frequent, they can become a performance issue. 
IVSHMEM is a way to avoid these VM Exits as no trap to the host is necessary. Each machine can simply read and write to the shared address space without needing to concern itself with the other. Additionally, once data is written to the region it is "in memory" which avoids unnecessary copies of data.

The split ring is a lockless producer consumer solution that operates half on the guest, and half on the host. It can be thought of as each side of a simplex connection, one going from guest to host, and the other from host to guest. RPC calls are set up in advance, through the split ring. 

## Instructions

Begin by creating a virtual machine environment. This project was tested and built on Ubuntu Server 16.04. IVSHMEM is integrated into QEMU/KVM.


The host needs to be started first. 

#### Thank You
A thank you to my supervisor Dr. Cameron MacDonell, who wrote the Nahanni IVSHMEM driver that made this project possible.
