/**********************************************************************
 * Redis Management                                                    *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: November 14, 2017                                          *
 * Modified: November 24, 2017                                         *
 ***********************************************************************/

/************* NOTE ***************************/
/* Code derived from examples in Hiredis docs */

#ifndef REDIS_CONNECT
#define REDIS_CONNECT
#include <hiredis/hiredis.h>

typedef enum {
	GEN_ERR =-1,
	INCR = 1,
	SET = 2,
	FETCH = 3
} Commands;

//connect to a redis server somewhere...
redisContext * redisNewConnect(const char * IP, const int port);

//increment a variable in redis
uint64_t increment(redisContext *c, const char * variable);

//set a key in redis
char * setKey(redisContext *c, const char * key, const char * val);

char * fetch(redisContext *c, const char * key);
#endif
