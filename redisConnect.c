/**********************************************************************
 * Redis Management                                                    *
 * Created By: Tobias Renwick (qubies)                                 *
 * Created: November 14, 2017                                          *
 * Modified: November 24, 2017                                         *
 ***********************************************************************/

/*#include "redisConnect.h"
#include <stdlib.h>
*/

#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include <hiredis/hiredis.h>
#include "redisConnect.h"
redisContext * redisNewConnect(const char * IP, const int port) {
	redisContext *c = NULL;
	struct timeval timeout = { 1, 500000 }; // 1.5 seconds
	c = redisConnectWithTimeout(IP, port, timeout);
	if (c == NULL || c->err) {
		if (c) {
			printf("Connection error: %s\n", c->errstr);
			redisFree(c);
		} else {
			printf("Connection error: can't allocate redis context\n");
		}
		exit(EXIT_FAILURE);
	}
	return c;
}

uint64_t increment(redisContext *c, const char * variable) {
	redisReply * reply = redisCommand(c,"INCR %s", variable);
	uint64_t val = (uint64_t)reply->integer;
    	freeReplyObject(reply);
	return val;
}

char * setKey(redisContext *c, const char * key, const char * val) {
	redisReply * reply = redisCommand(c,"SET %s %s", key, val);
	ssize_t replyLength = strlen(reply->str);
    	char * ans = malloc(replyLength+1);
	memcpy(ans, reply->str,replyLength+1 );
	freeReplyObject(reply);
	return ans;
}

char * fetch(redisContext *c, const char * key) {
	redisReply * reply = redisCommand(c,"GET %s", key);
	if (reply->str == NULL) {
		return NULL;
	}
	ssize_t replyLength = strlen(reply->str);
	char * ans = malloc(replyLength+1);
	memcpy(ans, reply->str,replyLength+1);
	freeReplyObject(reply);
	return ans;
}

